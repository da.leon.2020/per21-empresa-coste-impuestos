class employee:
    def __init__ (self, name, salary, tax, service):
        self.name=name
        self.salary=salary
        self.tax=tax
        self.service=service
    
    def CalculoImpuestos(self):
        if self.service<=12:
            self.employee_taxes=self.salary*self.tax
        else:
            self.discount= self.tax*0.10
            self.employee_taxes=self.salary*(self.tax-self.discount)
        print("El empleado {name} debe pagar {tax:.2f}".format(name=self.name, tax=self.employee_taxes))    
        return self.employee_taxes


employee1=employee('Sofia',1000,0.1,13)
employee2=employee('Dani',2000,0.2,4)
employee3=employee('Teresa',3000,0.3,8)
total=0
employee_list=[employee1,employee2,employee3]

for emp in employee_list:
    total+= emp.CalculoImpuestos()

print("Los impuestos a pagar en total son {:.2f} euros".format(total))
