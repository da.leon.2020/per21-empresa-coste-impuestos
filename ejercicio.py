nominas = [20000, 30000, 50000]
empleados = ["Pepe", "Teresa", "Sofia"]
lista_impuestos = [0.30, 0.20, 0.10]
meses = [12, 8, 16]

def CalculoImpuestos(nominas, empleados, lista_impuesto):
    total_impuestos = 0
    for index_empleado in range (len(empleados)): 
        nomina = nominas[index_empleado]
        imp = lista_impuestos[index_empleado]
        nombre = empleados[index_empleado]
        impuestos = nomina*imp
        if meses[index_empleado] >= 12:
            descuento = impuestos*0.10
            impuestos -= descuento
            total_impuestos += impuestos
        else:    
            total_impuestos += impuestos
        print("El empleado {name} debe pagar {tax:.2f}".format(name=nombre, tax=impuestos))
    return total_impuestos 

def displayCost(total):
    print("Los impuestos a pagar en total son {:.2f} euros".format(total))


displayCost(CalculoImpuestos(nominas, empleados, lista_impuestos))
