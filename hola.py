import sys

def main(argv):
    arg1 = argv[1]
    arg2 = argv[2]
    return float(arg1) + float(arg2)

if __name__ == "__main__":
    suma = main(sys.argv)
    print(suma)
